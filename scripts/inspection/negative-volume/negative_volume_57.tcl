set datafile public/cad/pockets/test-shoulder_04.step

# Read input geometry.
set datadir $env(ASI_TEST_DATA)
clear
load-step $datadir/$datafile

# Compute negative volume
compute-negative-volume -oneSolid -fids 1 2 47

set-as-part "negativeVolumeShape 1"

test-check-part-shape

test-check-solids-volumes 1.0e-4 19063.352691965763 

test-check-number-shape-entities -vertex 27 -edge 46 -wire 21 -face 21 -shell 1 -solid 1 -compsolid 0 -compound 0

test-check-shape-aabb-dim -xDim 15.000000000000004 -yDim 90 -zDim 20 -tol 1.0e-4

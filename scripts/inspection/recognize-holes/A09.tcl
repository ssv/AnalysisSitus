source $env(ASI_TEST_SCRIPTS)/inspection/recognize-holes/__begin

# Set working variables.
set datafile public/cad/holes/80841-328415-Kollektor2-lathe.stp
set radius 1e10
set refFids { 1 3 4 7 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 31 32 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 81 82 83 84 85 86 87 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 199 }

__recognize-holes

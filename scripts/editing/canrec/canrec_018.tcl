source $env(ASI_TEST_SCRIPTS)/editing/canrec/__begin

# Set working variables.
set datafile public/cad/milled/milled_002.stp

# Reference numbers.
set ref_nbSurfBezier     0
set ref_nbSurfSpl        14
set ref_nbSurfConical    43
set ref_nbSurfCyl        41
set ref_nbSurfOffset     0
set ref_nbSurfSph        2
set ref_nbSurfLinExtr    3
set ref_nbSurfOfRevol    2
set ref_nbSurfToroidal   39
set ref_nbSurfPlane      39
set ref_nbCurveBezier    0
set ref_nbCurveSpline    72
set ref_nbCurveCircle    277
set ref_nbCurveEllipse   0
set ref_nbCurveHyperbola 0
set ref_nbCurveLine      175
set ref_nbCurveOffset    0
set ref_nbCurveParabola  0

__convert-canonical

source $env(ASI_TEST_SCRIPTS)/editing/canrec/__begin

# Set working variables.
set datafile public/cad/sheet-metal/rear_camera_mount.stp

# Reference numbers.
set ref_nbSurfBezier     0
set ref_nbSurfSpl        2
set ref_nbSurfConical    0
set ref_nbSurfCyl        22
set ref_nbSurfOffset     0
set ref_nbSurfSph        0
set ref_nbSurfLinExtr    0
set ref_nbSurfOfRevol    0
set ref_nbSurfToroidal   0
set ref_nbSurfPlane      34
set ref_nbCurveBezier    0
set ref_nbCurveSpline    4
set ref_nbCurveCircle    44
set ref_nbCurveEllipse   0
set ref_nbCurveHyperbola 0
set ref_nbCurveLine      92
set ref_nbCurveOffset    0
set ref_nbCurveParabola  0

__convert-canonical

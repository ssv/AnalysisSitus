<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: bend sequence simulation</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->
  
  <!--
    Quick navigation script. Use a div with "toc-panel" class having a
    nested div with "toc" id to place the navigation panel.
  -->
  <script src="../js/jquery-3.5.1.min.js"></script>
  <script src="../js/toc.js"></script>

  <!-- [begin] Code highlight -->
  <link rel="stylesheet" href="../css/highlight.min.css">
  <script src="../js/highlight.min.js"></script>
  <!-- [end] Code highlight -->

  <!-- [begin] Pseudocode -->
  <script src="https://cdn.jsdelivr.net/npm/mathjax@2.7.9/MathJax.js?config=TeX-AMS_CHTML"
          integrity="sha256-DViIOMYdwlM/axqoGDPeUyf0urLoHMN4QACBKyB58Uw="
          crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            displayMath: [['$$','$$'], ['\\[','\\]']],
            processEscapes: true,
            processEnvironments: true,
        }
    });
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js"></script>
  <!-- [end] Pseudocode -->

</head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../featuresext.html">extensions</a>/bend sequence simulation
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='https://analysissitus.org/forum/index.php?resources/analysis-situs.6/'>Download</a></td>
  <td class="header-menu">
    <div class="dropdown">
      <button class="dropbtn">Features ...</button>
      <div class="dropdown-content">
        <a href="../features.html">Open source</a>
        <a href="../featuresext.html">Commercial</a>
      </div>
    </div>
  </td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-simulation">Bend sequence simulation</h1>

<div class="toc-panel"><div id="toc"></div></div>

<h2 id="toc-simulation-find-seq">Sequence finder</h2>

<p>
  Finding bend sequence is NP-hard problem. Therefore, SMRU employs a heuristic-based approach to find a reasonable solution. To check if a part is feasible or not, SMRU attempts to guess the order in which it might be folded. The pseudocode below describes the bend sequence finder's logic.
</p>

<p>
  The algorithm progressively identifies all bends that can be regarded as "last" ones to obtain the next intermediate folded state of geometry $S$. All feasible bends are reported in a single group and unfolded to generate a new folded state. The process repeats until the part gets completely unfolded or there remains no feasible bend to report. To conclude if the part is feasible it is enough to assure that all bends were listed in the collected groups.
</p>

<!-- pseudocode.js -->
<pre id="find-sequence" class="pseudocode">
  % Find feasible bends (www.analysissitus.org).
  \begin{algorithm}
  \caption{FindSequence}
  \begin{algorithmic}
  \PROCEDURE{FindSequence}{$S, B, \alpha$}
    \STATE \Comment{$S$ is the folded state of geometry.}
    \STATE \Comment{$B$ is a set of all bends.}
    \STATE \Comment{$\alpha$ is angular increment for collision frames.}
    \STATE stop $\gets$ false \Comment{Prepare iterations.}
    \STATE sequence $\gets \emptyset$ \Comment{Collected sequence.}
    \STATE processed $\gets \emptyset$ \Comment{Visited bends.}
    \REPEAT
      \STATE feasible $\gets$ $\emptyset$ \Comment{All bends which are feasible on this iteration.}
      \FOR{$b \subset B$} \Comment{Let's collect all feasible bends for the current state of $S$.}
        \IF{$b \in$ processed}
          \CONTINUE
        \ENDIF
        \IF { \CALL{IsBendFeasible}{$b$} }
          \STATE feasible $\gets b$
          \STATE processed $\gets b$
        \ENDIF
      \ENDFOR
      \IF{feasible = $\emptyset$} \Comment{No feasible bend remains.}
        \STATE stop $\gets$ true
        \CONTINUE
      \ENDIF
      \STATE $S$ $\gets$ \CALL{Unfold}{feasible, $\alpha$} \Comment{Unfold feasible bends.}
      \STATE sequence $\gets$ feasible
    \UNTIL{ !stop }
    \RETURN sequence
  \ENDPROCEDURE
  \end{algorithmic}
  \end{algorithm}
</pre>

<p>
  The <span class="code-inline">FindSequence()</span> function is heuristic-based and seeks a "generic" solution to the problem. Because the reported bends are not ordered in the outcome groups, the resulting sequences remain essentially ambiguous. Each group has a number of bends, each of which can be considered the "final" one together with all others in the same group. To solve ambiguity, the grouped bends should be numbered, i.e., an unordered tuple needs to become ordered. One solution is just to take the persistent order of bends in the group or, alternatively, use some "smarter" heuristics such as "shape determining weights" to sort bends. The imposed ordering is employed in the <span class="code-inline">Unfold()</span> function. However, the chosen ordering does not guarantee that a tested sequence is feasible (though it usually is).
</p>

<p class="note">
  Further discussion of weights for sorting bends in a sequence can be found in our "Sheet Metal" series in the Manifold Geometry blog. Read on about "permutation operators" <a href="https://quaoar.su/blog/page/on-sheet-metal-unfolding-part-6">here</a>.
</p>

<p>
  If the sequence is found, then that part is feasible (as long as simulation is precise enough). If, however, a sequence is not found, then it does not generally mean the part is infeasible. What it rather means is that the bending sequence discovered by the <span class="code-inline">FindSequence()</span> function is not good, and there still might be another (better) sequence that remained unexplored. Like in the game of chess, there is a huge configuration space to explore, and the optimal strategy is challeging to find. If the simulator fails to find such a strategy, a dedicated warning code is issued by the algorithm, and the human engineer has to review the part for a manual feasibility check.
</p>

<h2 id="toc-simulation-tooling">Tooling</h2>

<h3 id="toc-simulation-tooling-punches">Punches</h3>

<p>
  SMRU provides data-driven architecture for specifying the geometry of punches used in simulation. The geometry of a punch is stored in a BREP file as a closed contour. The simulator reads all available contours from the directory with punches and builds swept solids (prisms) out of them. The height of the prism is chosen with respect to the corresponding bend length. Precomputed punch solids are cached for reuse in other bends having the same length.
</p>

<div align="center"><a href="../imgs/ext/smru-simulation-punch_01.png"><img src="../imgs/ext/smru-simulation-punch_01.png" width="60%"/></a></div>

<p>
  For each punch loaded in, the corresponding mirrored version is automatically generated. This is done to simulate a part being turned 180 degrees by an operator. After punch placement, the flanges to the left and to the right w.r.t. the bend line are rotated symmetrically. For example, if the bend angle is 90 degrees, the left and right sides of the part will go 45 degrees each.
</p>

<h3 id="toc-simulation-tooling-swivel-machines">Swivel machines</h3>

<p>
  Swivel bending machines are different from press brakes, so the punch-oriented collision simulation is not directly available for them. The following differences are to be taken into account:
</p>

<ul>
  <li class="fancy_ul_item">The flanges do not rotate symmetrically: while one side is fixed, the other makes the whole bending angle.</li>
  <li class="fancy_ul_item">There is no punch tool as such, but the upper part of the machine can be employed as a "punch" for collision testing (with different rules of placement).</li>
</ul>

<p>
  Simulation of swivel bending is out of scope of SMRU as of now.
</p>

<h2 id="toc-simulation-commands">Simulation commands</h2>

<p>
  To prepare the simulation model, use <span class="code-inline">sm-begin-simulation</span> command in the Active Script window. This command converts the original B-rep model to a simplified dynamic mesh-based model that allows for interactive manipulations and collision checks.
</p>

<div align="center"><a href="../imgs/ext/smru-simulation_02.png"><img src="../imgs/ext/smru-simulation_02.png" width="60%"/></a></div>

<p>
  Clicking over the interactive bend handles you can observe "left" and "right" flanges as divided by the selected bend line. These handles span over the detected bend groups, so you can easily guess which flanges are going to be <a href="https://quaoar.su/blog/page/on-sheet-metal-unfolding-part-11-bend-grouping">folded together</a> and which ones are simulated separately.
</p>

<div align="center"><a href="../imgs/ext/smru-simulation_03.png"><img src="../imgs/ext/smru-simulation_03.png" width="60%"/></a></div>

<p>
  To find a bend sequence (and this way to test the part for feasibility), use <span class="code-inline">sm-find-seq</span> command. The outcome groups of bends will be reported in the Logger:
</p>

<pre>
  0: (1 2 3 4)
  1: (5 6)
</pre>

<p>
  This output means that the bends from the group "0" should be unfolded before the bends from the group "1". The simulator does not know which order to use in both groups, but it assumes that the reported orders are fine as long as they do not result in any collisions. You may want to trace the convergence of sequence finder visually. For that, pass the <span class="code-inline">-draw</span> flag to the <span class="code-inline">sm-find-seq</span> command:
</p>

<pre class="code">
> sm-begin-simulation
> sm-find-seq -draw
</pre>

<p>
  The following animation shows how the first group of feasible bends is populated. All bends except for the 5 and 6 can be regarded as final ones. The bends 5 and 6 require the first group to be done first as otherwise they cannot be approached without collisions. Therefore, the bends 5 and 6 are reported in the second group.
</p>

<div align="center"><a href="../imgs/ext/sm-simulation_find-seq.gif"><img src="../imgs/ext/sm-simulation_find-seq.gif" width="60%"/></a></div>

<p>
  Once the sequence is found, it is possible to replay it by using <span class="code-inline">sm-fold-seq</span> command.
</p>

<pre class="code">
> sm-begin-simulation
> sm-find-seq -draw
> sm-fold-seq -step 5 -draw -collideSelf -collidePunches -seq 1 2 3 4
</pre>

<p>
  Among the arguments of the <span class="code-inline">sm-fold-seq</span> command, you may want to configure the simulation angle and a couple of options to turn on and off the provided collision checkers: with punches and with the part itself. Use the <span class="code-inline">-seq</span> key to pass the IDs of the bends to unfold. Normally, you should use the IDs reported by the sequence finder but you are also free to experiment with different sequences if you will.
</p>

<div align="center"><a href="../imgs/ext/sm-simulation_fold-seq.gif"><img src="../imgs/ext/sm-simulation_fold-seq.gif" width="60%"/></a></div>

<p>
  To fold and unfold a selected bend, use <span class="code-inline">sm-fold</span> command followed by the angle increment. This command is mostly useful for maintenance but it also allows you to check closely punch positioning with respect to a bend line in a certain folded state of geometry.
</p>

<div align="center"><a href="../imgs/ext/sm-simulation_fold.gif"><img src="../imgs/ext/sm-simulation_fold.gif" width="60%"/></a></div>

<p>
  Punches store the detected collision zones making it possible to consult the detected interferences later one. Here is an example illustrating why bend 5 cannot be unfolded in the first group of bends. The punch profile has been loaded from filesystem and extruded along the bend line. On simulation, it has been posed at a bend line's <a href="https://quaoar.su/blog/page/on-sheet-metal-unfolding-part-4-1">border trihedron</a> and tested for collisions.
</p>

<div align="center"><a href="../imgs/ext/smru-simulation_04.png"><img src="../imgs/ext/smru-simulation_04.png" width="60%"/></a></div>

<p class="note">
  It should be noted that decreasing the angular step leads to more precise simulation, because collision testing is done at each frame. However, too small angular values have negative impact on the overall performance of the simulation. A reasonable degree to be used in automation is probably around 15 degrees.
</p>

<p>
  To avoid false-positive collisions for "boxy" shapes, the geometry of punches is slightly reduced as the following illustration shows:
</p>

<div align="center"><a href="../imgs/ext/smru-simulation_05.png"><img src="../imgs/ext/smru-simulation_05.png" width="60%"/></a></div>

<!-- Optional references to community resources -->
<div class="mg">
  <div class="mg-headline">Community reading:</div>
  <ul>
    <li class="fancy_ul_item">MG // <a href="https://quaoar.su/blog/page/on-sheet-metal-unfolding-part-6">On sheet metal unfolding. Part 6: bend sequences</a>.</li>
    <li class="fancy_ul_item">MG // <a href="https://quaoar.su/blog/page/on-sheet-metal-unfolding-part-9-dynamic-unfolding">On sheet metal unfolding. Part 9: dynamic unfolding</a>.</li>
  </ul>
</div>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    <small>&copy; Quaoar&nbsp;Studio&nbsp;LLC 2015-present &nbsp; (Yerevan, Armenia)</small> | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

<script>
hljs.highlightAll();
pseudocode.renderClass("pseudocode");
</script>

</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: SMRU flange checks</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->
  
  <!--
    Quick navigation script. Use a div with "toc-panel" class having a
    nested div with "toc" id to place the navigation panel.
  -->
  <script src="../js/jquery-3.5.1.min.js"></script>
  <script src="../js/toc.js"></script>

  <!-- [begin] Code highlight -->
  <link rel="stylesheet" href="../css/highlight.min.css">
  <script src="../js/highlight.min.js"></script>
  <!-- [end] Code highlight -->

  <!-- [begin] Pseudocode -->
  <script src="https://cdn.jsdelivr.net/npm/mathjax@2.7.9/MathJax.js?config=TeX-AMS_CHTML"
          integrity="sha256-DViIOMYdwlM/axqoGDPeUyf0urLoHMN4QACBKyB58Uw="
          crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            displayMath: [['$$','$$'], ['\\[','\\]']],
            processEscapes: true,
            processEnvironments: true,
        }
    });
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js"></script>
  <!-- [end] Pseudocode -->

</head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../featuresext.html">extensions</a>/SMRU flange checks
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='https://analysissitus.org/forum/index.php?resources/analysis-situs.6/'>Download</a></td>
  <td class="header-menu">
    <div class="dropdown">
      <button class="dropbtn">Features ...</button>
      <div class="dropdown-content">
        <a href="../features.html">Open source</a>
        <a href="../featuresext.html">Commercial</a>
      </div>
    </div>
  </td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-clearances">SMRU flange checks</h1>

<div class="toc-panel"><div id="toc"></div></div>

<p>
  To assess feasibility and potential production issues, SMRU takes additional measurements over each bend edge. Such measurements include the length of the flange and the distances to the nearest cutting curves. We also distinguish straight cutting lines parallel to the bend edge from other cutting curves. The next sections go over the key properties reported in each "clearance" JSON object in the <span class='code-inline'>clearances</span> array.
</p>

<h2 id="toc-clearances-search-range">Search range</h2>

<p>
  Each bend incorporates information on the properties of the flanges to the left and right w.r.t. the bend line. The essential measurements are taken from the straight line on the bend edge (rather than the center bend line), i.e., they correspond to the folded state of a part. The following image illustrates the "search range" chosen for an abstract trapezoidal flange:
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_01.png"><img src="../imgs/ext/bend-clr/bend-clr_01.png" width="40%"/></a></div>

<p>
  The "search range" is enclosed by a couple of trimming lines perpendicular to the bend line. To build a trimming (helper) line, the following logic is used:
</p>

<ul>
  <li class="fancy_ul_item">Extract a couple of the extremity vertices for the bend edge.
  <li class="fancy_ul_item">Check if a vertex is coincident with the corner of the 2D bounding box for a flange.
  <li class="fancy_ul_item">If the vertex is on the corner, another point is chosen by shifting the vertex point inwards by the distance $x$.
</ul>

<p>
  The shift $x$ allows to make slight offsets from the extremities of the flange: $x = c \cdot l_b$, where $c < 0.5$ is a predefined constant (calibrated within SMRU) and $l_b$ is the length of the bend edge. Without offsets, the measured closest points tend to be meaningless zeroes. If the extremity of the bend edge is not at the corner of the bounding box, no offset is applied as shown in the following image (see the right corner):
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_02.png"><img src="../imgs/ext/bend-clr/bend-clr_02.png" width="40%"/></a></div>

<h2 id="toc-clearances-outer-measurements">Distances to outer contour</h2>

<p>
  SMRU measures three different lengths for the outer contour w.r.t. the bend edge. The simplest is the length of the 2D bounding box aligned with the bend line, which yields the max distance, denoted as $b$ in the pictures below. This length influences whether the sheet can be positioned on the V-die for bending.
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_03.png"><img src="../imgs/ext/bend-clr/bend-clr_03.png" width="70%"/></a></div>

<p>
  The second measurement is the distance to the nearest straight line parallel to the bend edge. The distance is denoted as $a$. If there is no other edge on the face inside the search range that is parallel to the bend line, then $a$ has the value of null.
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_04.png"><img src="../imgs/ext/bend-clr/bend-clr_04.png" width="70%"/></a></div>

<p>
  The third length is the distance to the closest point on the outer contour within the search area, denoted as $c$.
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_05.png"><img src="../imgs/ext/bend-clr/bend-clr_05.png" width="70%"/></a></div>

<p>
  Below is one example of mapping these measurement to the folded 3D model. Here $a \equiv c$:
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_06.png"><img src="../imgs/ext/bend-clr/bend-clr_06.png" width="70%"/></a></div>

<p>
  Another example is when $a \neq c$:
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_07.png"><img src="../imgs/ext/bend-clr/bend-clr_07.png" width="70%"/></a></div>

<h2 id="toc-clearances-inner-measurements">Distances to inner contours</h2>

<p>
  Inner contours most typically correspond to holes and cutouts. If such features are located close to a bend line, they can get deformed on bending, so a certain clearance has to be respected. The search range for the inner contours is expanded to the extremities of the bend edge, i.e., $x \equiv 0$.
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_08.png"><img src="../imgs/ext/bend-clr/bend-clr_08.png" width="70%"/></a></div>

<p>
  It should be noted that the holes and cutouts falling outside the search range do not contribute to the reported values, because they are located in the "safe" zones w.r.t. possible deformations.
</p>

<div align="center"><a href="../imgs/ext/bend-clr/bend-clr_09.png"><img src="../imgs/ext/bend-clr/bend-clr_09.png" width="70%"/></a></div>

<h2 id="toc-clearances-support">Support edge</h2>

<p>
  A straight edge parallel to the bend line is needed for a part to properly contact the back gauge as shown in the following image. The corresponding check is added for manufacturability analysis.
</p>

<div align="center"><a href="../imgs/ext/smru-support-edge_01.png"><img src="../imgs/ext/smru-support-edge_01.png" width="60%"/></a></div>

<p>
  If the outer contour of the flange does not have at least one straight edge that is parallel to the bend line, the corresponding flange is flagged. Such an edge is called a "support edge."
</p>

<div align="center"><a href="../imgs/ext/smru-support-edge_02.png"><img src="../imgs/ext/smru-support-edge_02.png" width="60%"/></a></div>

<p>
  Below is an example of a part having no support edges at its flanges:
</p>

<div align="center"><a href="../imgs/ext/smru-support-edge_03.png"><img src="../imgs/ext/smru-support-edge_03.png" width="60%"/></a></div>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    <small>&copy; Quaoar&nbsp;Studio&nbsp;LLC 2015-present &nbsp; (Yerevan, Armenia)</small> | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

<script>
hljs.highlightAll();
pseudocode.renderClass("pseudocode");
</script>

</body>
</html>

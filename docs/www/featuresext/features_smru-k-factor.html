<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: K-factor</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->
  
  <!--
    Quick navigation script. Use a div with "toc-panel" class having a
    nested div with "toc" id to place the navigation panel.
  -->
  <script src="../js/jquery-3.5.1.min.js"></script>
  <script src="../js/toc.js"></script>

  <!-- [begin] Code highlight -->
  <link rel="stylesheet" href="../css/highlight.min.css">
  <script src="../js/highlight.min.js"></script>
  <!-- [end] Code highlight -->

  <!-- [begin] Pseudocode -->
  <script src="https://cdn.jsdelivr.net/npm/mathjax@2.7.9/MathJax.js?config=TeX-AMS_CHTML"
          integrity="sha256-DViIOMYdwlM/axqoGDPeUyf0urLoHMN4QACBKyB58Uw="
          crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            displayMath: [['$$','$$'], ['\\[','\\]']],
            processEscapes: true,
            processEnvironments: true,
        }
    });
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js"></script>
  <!-- [end] Pseudocode -->

</head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../featuresext.html">extensions</a>/K-factor
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='https://analysissitus.org/forum/index.php?resources/analysis-situs.6/'>Download</a></td>
  <td class="header-menu">
    <div class="dropdown">
      <button class="dropbtn">Features ...</button>
      <div class="dropdown-content">
        <a href="../features.html">Open source</a>
        <a href="../featuresext.html">Commercial</a>
      </div>
    </div>
  </td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-k-factor">Mastering k-factor</h1>

<div class="toc-panel"><div id="toc"></div></div>

<h2 id="toc-k-factor-overview">Overview</h2>

<p>
  Folding causes material deformations that are all concentrated at bend features. These deformations must be taken into account when unfolding a sheet metal part. The k-factor is a fundamental coefficient used to calculate bend allowances and predict material distortion. The k-factor defines the relative position of the "neutral axis" within the material's thickness. While unstressed (and flat), the neutral axis is located at 50% of the material thickness. During bending, the neutral axis shifts to the inside of the material. The neutral axis position is determined by the bend angle, inner bend radius, and method of forming.
</p>

<div align="center"><a href="../imgs/ext/smru-k-factor_02.png"><img src="../imgs/ext/smru-k-factor_02.png" width="50%"/></a></div>

<p>
  The default value of k-factor in SMRU is 0.4468, which is used in most bending applications. Usually, k-factor takes values from 0.5 all the way down to 0.33. The used k-factor values are customizable by inheritance from the base class named <span class="code-inline">kFactorFunc</span>. The provided interface assumes that k-factor is a function of the inner bend radius and the material thickness, i.e., $k = k(r,t)$.
</p>

<pre><code>class kFactorFunc : public Standard_Transient
{
public:

  //! Computes k-factor for the passed radius and thickness
  //! properties at a bend feature.
  //! \param[in] r the bend radius in radians.
  //! \param[in] t the material thickness in model units.
  //! \return the computed k-factor value.
  virtual double
    Eval(const double r, const double t) const = 0;
};
</code></pre>

<p>
  The following default implementations of the k-factor function are shipped with SMRU:
</p>

<ul>
  <li class="fancy_ul_item"><span class="code-inline">kFactorConstFunc</span> for $k \equiv \text{const}$</li>
  <li class="fancy_ul_item"><span class="code-inline">kFactorTabularFunc</span> for $k(r,t) = a \frac{r}{t} + b$, where the coefficients $a$ and $b$ are defined externally.</li>
</ul>

<p>
  For the reference, each bend is reported in the JSON outcome together with the k-factor used for unfolding that bend.
</p>

<div align="center"><a href="../imgs/ext/smru-k-factor_01.png"><img src="../imgs/ext/smru-k-factor_01.png" width="50%"/></a></div>

<p>
  The client application is free to define its custom k-factor by subclassing the base <span class="code-inline">kFactorFunc</span>, which can be implemented in whatever way. To set the custom k-factor function in SMRU, use the corresponding setter method of <span class="code-inline">InspectFile</span> class as the following code snippet shows:
</p>

<pre><code>// Prepare inspector.
InspectFile inspector(cadFilename, progress, nullptr);
//
inspector.SetKFactorFunc(kFactorFunc);</code></pre>

<p>
  Bend allowance $b_a$ is computed as $b_a = r_n \phi$, where $r_n$ is the bend radius at the neutral axis and $\phi$ is the bend angle in radians. Here $r_n = r_i + k(r,t) \cdot t$, where $r_i$ is the inner radius of the bend. Therefore, bend allowance is nothing but the length of the circular arc at the neutral axis.
</p>

<h2 id="toc-k-factor-pass">Pass k-factor to SMRU</h2>

<p>
  To configure the k-factor used in the <span class="code-inline">sm-perform</span> command or in the <span class="code-inline">asiSheetMetalExe</span> executable, pass the <span class="code-inline">-k</span> parameter followed by the desired value.
</p>

<pre><code>> sm-perform -filename azv_clamp.stp -k 0.33</code></pre>

<p>
  A simple set of materials that specify empirical laws for k-factor are included with SMRU for rapid prototyping. Our goal was to provide a means of quick testing for people who know the material of the part but are not familiar with the concept of k-factor. The following basic materials are defined with their short names:
</p>

<ol>
  <li class="fancy_ul_item">"SOFT_ALU"</li>
  <li class="fancy_ul_item">"HARD_ALU"</li>
  <li class="fancy_ul_item">"STEEL"</li>
  <li class="fancy_ul_item">"STAINLESS"</li>
  <li class="fancy_ul_item">"BRASS"</li>
  <li class="fancy_ul_item">"COPPER"</li>
</ol>

<p>
  Each item from the list comes with its associated coefficients $a$ and $b$ used in the functional definition of k-factor as $k(t,r) = a \frac{r}{t} + b$. Therefore, for the fixed material and thickness, k-factor is a linear function of the inner radius $r$.
</p>

<table align="center" style="border: 1px solid rgb(100, 100, 100);" cellpadding="5" cellspacing="0" width="100%">
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">SOFT_ALU</span>
    </td>
    <td class="table-content-block">
      <p>
        $a = 0.185, b = 0.2875$
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">HARD_ALU</span>
    </td>
    <td class="table-content-block">
      <p>
        $a = -0.005786, b = 0.4156$
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">STEEL</span>
    </td>
    <td class="table-content-block">
      <p>
        $a = -0.03395, b = 0.4831$
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">STAINLESS</span>
    </td>
    <td class="table-content-block">
      <p>
        $a = -0.068, b = 0.4980$
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">BRASS</span>
    </td>
    <td class="table-content-block">
      <p>
        $a = -0.005786, b = 0.4156$
      </p>
    </td>
  </tr>
  <tr>
    <td align="center" class="table-content-header" width="160px">
      <span class="code-inline">COPPER</span>
    </td>
    <td class="table-content-block">
      <p>
        $a = -0.005786, b = 0.4156$
      </p>
    </td>
  </tr>
</table>

<p>
  The <span class="code-inline">kFactorTabularFunc</span> definition is used for those provided materials. The above technique illustrates how an embedded k-factor "calculator" that takes into account the "know how" of a particular manufacturing shop may be used with the unfolding algorithm.
</p>

<!-- Optional references to community resources -->
<div class="mg">
  <div class="mg-headline">Community reading:</div>
  <ul>
    <li class="fancy_ul_item">MG // <a href="https://quaoar.su/blog/page/on-sheet-metal-unfolding-part-3">On sheet metal unfolding (Part 3): mastering k-factor</a>.</li>
  </ul>
</div>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    <small>&copy; Quaoar&nbsp;Studio&nbsp;LLC 2015-present &nbsp; (Yerevan, Armenia)</small> | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

<script>
hljs.highlightAll();
pseudocode.renderClass("pseudocode");
</script>

</body>
</html>

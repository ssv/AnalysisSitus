<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Analysis Situs: visualization modes</title>
  <link rel="shortcut icon" type="image/png" href="../imgs/favicon.png"/>
  <link rel="stylesheet" type="text/css" href="../css/situ-main-style.css">

  <!-- [begin] Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112292727-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-112292727-2');
  </script>
  <!-- [end] Google Analytics -->

  <!--
     Quick navigation script. Use a div with "toc-panel" class having a
     nested div with "toc" id to place the navigation panel.
    -->
    <script src="../js/jquery-3.5.1.min.js"></script>
    <script src="../js/toc.js"></script>

  <!-- [begin] Code highlight -->
  <link rel="stylesheet" href="../css/highlight.min.css">
  <script src="../js/highlight.min.js"></script>
  <!-- [end] Code highlight -->

  <!-- [begin] Pseudocode -->
  <script src="https://cdn.jsdelivr.net/npm/mathjax@2.7.9/MathJax.js?config=TeX-AMS_CHTML"
          integrity="sha256-DViIOMYdwlM/axqoGDPeUyf0urLoHMN4QACBKyB58Uw="
          crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script type="text/x-mathjax-config">
    MathJax.Hub.Config({
        tex2jax: {
            inlineMath: [['$','$'], ['\\(','\\)']],
            displayMath: [['$$','$$'], ['\\[','\\]']],
            processEscapes: true,
            processEnvironments: true,
        }
    });
  </script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.css">
  <script src="https://cdn.jsdelivr.net/npm/pseudocode@latest/build/pseudocode.min.js"></script>
  <!-- [end] Pseudocode -->

</head>
<body>

<a name="top"></a> <!-- anchor for 'back to top' link -->
<table width="100%"><tr><td>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td align="left" class="header">
    <span class="header-logo"><a href="../index.html" class="header-href">Analysis&nbsp;Situs</a></span>
    &nbsp;
    ./<a class="header-href" href="../features.html">features</a>/visualization modes
  </td>
</tr>
</table>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td class="header-menu"><a href='https://analysissitus.org/forum/index.php?resources/analysis-situs.6/'>Download</a></td>
  <td class="header-menu">
    <div class="dropdown">
      <button class="dropbtn">Features ...</button>
      <div class="dropdown-content">
        <a href="../features.html">Open source</a>
        <a href="../featuresext.html">Commercial</a>
      </div>
    </div>
  </td>
  <td class="header-menu"><a href="https://gitlab.com/ssv/AnalysisSitus">Source code</a></td>
  <td class="header-menu"><a href="http://analysissitus.org/forum/index.php">Forum</a></td>
  <td class="header-menu"><a href="http://quaoar.su/blog/contact">Ask for support</a></td>
</tr>
</table>
</td></tr></table>

<div class="content-body">
<!-- [BEGIN] contents -->

<h1 id="toc-modes">Visualization modes</h1>

<div class="toc-panel"><div id="toc"></div></div>

<p>
  The visualization module of Analysis Situs is based on VTK (Visualization Toolkit) <a href="../references.html#schroeder-2006">[Schroeder, 2006]</a>, which is a widely-used and industrially proven open-source library. Analysis Situs adds the CAD-oriented data processing <a href="./features_visu-pipelines.html">pipelines</a> to specialize the generic VTK engine for Computer-Aided Design use cases. The main contribution of Analysis Situs in this regard is adding a set of data sources that convert precise B-rep geometries to the polygonal format of VTK. In this chapter, we give an overview of the visualization techniques available in Analysis Situs.
</p>

<h2 id="toc-modes-pm">Parameter editor</h2>

<p>
  By default, Analysis Situs comes up with a shaded display mode having edges rendering enabled.
</p>

<div align="center"><img src="../imgs/shading-dm.png"/></div>

<p>
  The display mode of the active CAD part can be tuned in the Parameter Editor.
</p>

<div align="center"><img src="../imgs/dm-parameter-editor.png"/></div>

<p>
  One important setting that is related to both visualization and interactive selection is the "Use scalars" flag. If disabled, the per-element colors are not rendered. Since the highlighting mechanism is also based on scalars, it will not show up as well. The following image illustrates the rendering view without scalars. As you can see, shape anomalies are not accented anymore with the red color.
</p>

<div align="center"><img src="../imgs/shading-dm-noscalars.png"/></div>

<h2 id="toc-modes-facets">Facets</h2>

<p>
  To enable the visualization of the B-rep facets backing the precise geometry, press "W" ("wireframe") key whenever the focus is set to the viewer. To get back to the shaded view, press "S" ("shading").
</p>

<div align="center"><img src="../imgs/mesh-hlr.png"/></div>

<p>
  The "W"/"S" toggle is applied only to the actors that are currently visible on the scene. If you want to keep the visualization of facets enabled for a dedicated object while keeping others shaded, temporarily hide them from the scene and use the toggle.
</p>

<h2 id="toc-modes-hlr">Hidden line removal (HLR)</h2>

<p>
  To render the current Part's hidden line view, press the "H" ("hidden") key. The "Alt + H" combination is reserved for a discrete HLR that is intended to perform faster at the cost of precision loss. Pressing the "H" button starts HLR in two threads: one for the "precise" mode and one for the discrete mode. The application sets up a timeout for both threads. You may configure this timeout (in milliseconds) in the root "Analysis Situs" Node. Often, the precise HLR exceeds the timeout, while the discrete mode does not. In this case, the discrete HLR view will be displayed. If you wish to enforce the precise mode, consider increasing the default timeout (0.5 seconds).
</p>

<div align="center"><img src="../imgs/brep-hlr-precise.png"/></div>

<p>
  To preserve the HLR projection of a part with all hidden edges shown, navigate to the root Node ("Analysis Situs" in the project tree) and enable the "Project hidden edges" flag:
</p>

<div align="center"><a href="../imgs/precise-hlr-setting.png"><img src="../imgs/precise-hlr-setting.png" width="50%"/></a></div>

<p>
  For example, a planar part with normal HLR is projeced as follows:
</p>

<div align="center"><img src="../imgs/precise-hlr-clean.png"/></div>

<p>
  Enabling the extraction option for the hidden edges allows you to view certain potentially unexpected features in the part's thickness:
</p>

<div align="center"><img src="../imgs/precise-hlr-hidden.png"/></div>

<p>
  To disable vertice visualization for the projection, pick the "HLR" Node in the project tree and set the display mode to "Pure wireframe":
</p>

<div align="center"><img src="../imgs/precise-hlr-pure-wireframe.png"/></div>

<p>
  The visualization results become cleaner, i.e., the vertices of the projected edges are no longer rendered.
</p>

<div align="center"><img src="../imgs/precise-hlr-hidden-clean.png"/></div>

<h2 id="toc-modes-hlr-box">"Black box" HLR</h2>

<div align="center"><a href="../imgs/bbox-hlr-part.png"><img src="../imgs/bbox-hlr-part.png" width="60%"/></a></div>

<p>
  Press "B" (precise) or "Alt + B" (discrete) to view all HLR projections in "black box" mode. All six projection views are created and placed on the sides of the active part's bounding box. It should be noted that box projection uses the configured timeout for each of 6 HLR invocations.
</p>

<div align="center"><img src="../imgs/bbox-hlr.gif"/></div>

<h2 id="toc-modes-aag">AAG</h2>

<h3 id="toc-modes-aag-formal">Graph view</h3>

<p>
  To explore AAG of the current part, click one of the "Show AAG..." buttons from the "Analysis" tab in the UI. Clicking these buttons opens a dialog window in which AAG is represented as an undirected graph, with nodes representing CAD faces and arcs representing their adjacency relationships.
</p>

<div align="center"><a href="../imgs/aag-parameter-editor.png"><img src="../imgs/aag-parameter-editor.png" width="60%"/></a></div>

<p>
  Each graph arc is colored to report the dihedral angle type according to the following color codes:
</p>

<table>
  <tr>
    <td class="code-cell" style="background-color: #ff5136; color: black;">Concave</td>
  </tr>
  <tr>
    <td class="code-cell" style="background-color: #68C800; color: black;">Convex</td>
  </tr>
  <tr>
    <td class="code-cell" style="background-color: #bcbcbc; color: black;">Undefined</td>
  </tr>
</table>

<p>
  The graph nodes are selectable: picking a node in the graph view highlights the corresponding face in the active part.
</p>

<h3 id="toc-modes-aag-embedded">Embedded view</h3>

<p>
  Turning on the "Render AAG" checkbox in the Part Node allows you to see AAG in the 3D scene alongside the active part.
</p>

<div align="center"><a href="../imgs/aag_embedded.png"><img src="../imgs/aag_embedded.png" width="60%"/></a></div>

<p>
  This option does not allow you to interactively select the graph nodes or change the graph layout anyhow.
</p>

<!-- [END] contents -->
</div>
<br/>
<table class="footer" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td>
    <small>&copy; Quaoar&nbsp;Studio&nbsp;LLC 2015-present &nbsp; (Yerevan, Armenia)</small> | &nbsp; <a class="footer-href" href="#top">^^^</a>  &nbsp; | &nbsp; <a href="http://quaoar.su/blog/contact" class="footer-href">contact us</a> &nbsp; | &nbsp; <a href="https://www.youtube.com/channel/UCc0exKIoqbeOSqKoc1RnfBQ" class="icon brands fa-youtube"></a> &nbsp; | &nbsp; <a href="https://quaoar.su" class="icon brands fa-medium"></a> &nbsp; | &nbsp; <a href="https://www.linkedin.com/in/sergey-slyadnev-277496b1" class="icon brands fa-linkedin"></a>
  </td>
</tr>
</table>

<script>
hljs.highlightAll();
pseudocode.renderClass("pseudocode");
</script>

</body>
</html>
